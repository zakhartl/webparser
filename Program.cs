﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using WebParser;


namespace WebParser
{
    class Program
    {
        static void Main( string[] args )
        {
            Console.WriteLine( "Введите адрес сайта для подсчета уникальных слов" );
            string urlAddress = Console.ReadLine();
            urlAddress = urlAddress.Length == 0 ? "yandex.ru" : urlAddress;
            var parser = new ContentParser( new FileLogger() );
            parser.Parse( urlAddress );
        }
    }
}
