﻿using System;

namespace WebParser
{
    public interface ILogMessage
    {
        string Message { get; }
        Exception Exception { get; }
    }
}
